####Herbivorous

* Horse
* Cow
* Sheep
* Capybara

####Carnivorous

* Cat
* Dog
* Polar Bear
* Lion
* Frog
* Hawk

####Omnivorous

* Rat
* Fox
* Raccoon
* Chicken
* Fennec Fox
